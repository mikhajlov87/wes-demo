import * as React from 'react';
import { observer, inject } from 'mobx-react';
import * as classnames from 'classnames';
import { Field, FieldRenderProps } from 'react-final-form';
import transl from '../../../utils/t.decorator';
import { GenericProps } from '../../../interfaces/GenericProps.interface';
import { PackageCreateStore } from '../../../stores/PackageCreate.store';
import { NewPackage } from '../../../services/api';
import TypeEnum = NewPackage.TypeEnum;
import {RadioGroup, Radio} from 'react-icheck';

interface PackageCreateSelectPlanComponentProps extends GenericProps {
    packageCreateStore?: PackageCreateStore;
}

@transl()
@inject('packageCreateStore')
@inject('userStore')
@observer
export class PackageCreateSelectPlanComponent extends React.Component<PackageCreateSelectPlanComponentProps> {
    render(): React.ReactNode {
        return (
            <Field
                name="plan"
                type="radio"
                render={(args) => this.renderSelectPlan(args)}
            />
        );
    }

    renderSelectPlan(args: FieldRenderProps): React.ReactNode {
        const { t } = this.props;
        const { INDIVIDUAL, GROUP, EVALUATION, TRIAL } = TypeEnum;

        return (
            <div className="row select-plan">
                {
                    this.props.userStore.trialMode
                    ? <div className="form-group col-md-3">
                        {this.renderRadioButton(TRIAL, 'Trial', `${t('Free trial lesson to')} <br /> ${t('test our platform')}`, args)}
                    </div>
                    : <div className="col-md-1"></div>
                }
                <div className="form-group col-md-3">
                    {this.renderRadioButton(INDIVIDUAL, 'Individual', `${t('Create an individual')} <br /> ${t('lesson for each')} <br /> ${t('student')}`, args)}
                </div>
                <div className="form-group col-md-3">
                    {this.renderRadioButton(GROUP, 'Group', `${t('Create a group lesson')} <br /> ${t('and invite your')} <br /> ${t('employees')}`, args)}
                </div>
                <div className="form-group col-md-3">
                    {this.renderRadioButton(EVALUATION, 'Evaluation', `${t('Take an English Level')} <br /> ${t('Test for your')} <br /> ${t('employees')}`, args)}
                </div>


            </div>
        );
    }

    renderRadioButton(value: TypeEnum, title: string, paragraph: string, fieldRenderProps: FieldRenderProps): React.ReactNode {
        const { t, packageCreateStore } = this.props;
        const { selectedPlan } = packageCreateStore;
        const { input } = fieldRenderProps;
        const isChecked: boolean = selectedPlan === value;
        const classNames: string = classnames({
            ['radio']: true,
            ['active-state']: isChecked,
        });

        return (
            <div className={classNames}>
                <label>
                    <input
                        disabled={this.props.userStore.trialMode && value !== TypeEnum.TRIAL}
                        {...input}
                        type="radio"
                        checked={isChecked}
                        name="optionsRadio"
                        value={value}
                        onClick={() => { this.changeSelectedPlan(value); }}
                    /> &nbsp;
                    {t(title)}
                    <p dangerouslySetInnerHTML={{ __html: paragraph }} />
                </label>
            </div>
        );
    }

    changeSelectedPlan(value: TypeEnum): void {
        this.props.packageCreateStore.changeSelectedPlan(value);
    }
}
