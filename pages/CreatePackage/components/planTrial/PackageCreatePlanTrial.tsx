import * as React from 'react';
import { FormFieldNameTitleComponent } from '../formFields/FormFieldNameTitle.component';
import { FormFieldNameStudentsComponent } from '../formFields/FormFieldNameStudents.component';
import { PackageCreateChoosedStudentsComponent } from '../PackageCreateChoosedStudents.component';
import { FormFieldNameStartWeekComponent } from '../formFields/FormFieldNameStartWeek.component';
import { FormFieldNameNumberHoursComponent } from '../formFields/FormFieldNameNumberHours.component';
import { FormFieldNameNumberWeekComponent } from '../formFields/FormFieldNameNumberWeek.component';

interface PackageCreatePlanTrialProps {
}
interface PackageCreatePlanTrialState {
}

export class PackageCreatePlanTrial extends React.Component<PackageCreatePlanTrialProps, PackageCreatePlanTrialState> {
    render(): React.ReactNode {
        return [
            <div className="row" key="title">
                <FormFieldNameTitleComponent col="col-md-6" />
                <FormFieldNameStudentsComponent col="col-md-6" />
            </div>,
            <PackageCreateChoosedStudentsComponent key="choosed" />,
        ];
    }
}
