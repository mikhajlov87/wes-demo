import * as React from 'react';
import { GenericProps } from '../interfaces/GenericProps.interface';
import { Switch, Route } from 'react-router';
import { NotificationRegularPage } from './Notifications/NotificationsRegular';
import { NotificationAlertPage } from './Notifications/NotificationsAlerts';
import transl from '../utils/t.decorator';
import { NavLink } from 'react-router-dom';
import { NotificationInviteUserPage } from './Notifications/NotificationsInviteUser';

export interface NotificationPageProps extends GenericProps {
}
export interface NotificationPageState {
}
@transl()
export class NotificationPage extends React.Component<NotificationPageProps, NotificationPageState> {
    render(): JSX.Element {
        const {t} = this.props;
        return (
            <div className="row">
                <div className="col-md-12">
                    <h1>{t('Notifications')}</h1>
                    <ul className="nav nav-tabs">
                        <li><NavLink exact to="/notifications/">{t('Regular')}</NavLink></li>
                        <li><NavLink exact to="/notifications/alerts">{t('Alerts')}</NavLink></li>
                        <li><NavLink exact to="/notifications/invite-user">{t('Invites')}</NavLink></li>
                    </ul>
                </div>
                <div className="col-md-12">
                <section className="notification-list">
                <Switch>
                    <Route exact path="/notifications/" component={NotificationRegularPage}/>
                    <Route exact path="/notifications/alerts" component={NotificationAlertPage}/>
                    <Route exact path="/notifications/invite-user" component={NotificationInviteUserPage}/>
                </Switch>
                </section>
                </div>
            </div>

        );
    }
}
