import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { GenericProps } from '../../../../interfaces/GenericProps.interface';
import transl from '../../../../utils/t.decorator';
import LazyLoad from '../../../../components/LazyLoad';
import { UsersExtended, User } from '../../../../services/api';
import UserThumbComponent from '../../../components/UserThumb.component';

export interface ChatMainContactListProps extends GenericProps {
}

@transl()
@inject('chatStore')
@observer
export class ChatMainContactList extends React.Component<ChatMainContactListProps> {
    render(): React.ReactNode {
        const { chatStore } = this.props;
        const users$: User[] = chatStore.users$.current();

        return (
            <section className="contact-list">
                <ul>
                    <LazyLoad $={users$}>
                        {() => users$.map(this.renderContactListItem)}
                    </LazyLoad>
                </ul>
            </section>
        );
    }

    renderContactListItem = (user: User): React.ReactNode => {
        const { name, photo, id } = user;

        return (
            <li key={id}>
                <a href="#" onClick={(e) => { this.setCurrentReceipentId(e, id); }}>
                    <span className="avatar">
                        <UserThumbComponent user={user} />
                    </span>
                    <span className="info">
                        <span className="last">{}</span> {/* TODO: ADD LAST MESSAGE */}
                        <span className="name">{name}</span>
                        <span className="status">{}</span> {/* TODO: ADD STATUS */}
                    </span>
                </a>
            </li>
        );
    }

    setCurrentReceipentId = (event: React.SyntheticEvent<HTMLAnchorElement>, id: number): void => {
        const { chatStore } = this.props;

        chatStore.setCurrentReceipentId(id);
        event.preventDefault();
    }
}
