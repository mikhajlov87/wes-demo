import { observable, action, runInAction, computed, reaction } from 'mobx';
import { lazyObservable, ILazyObservable } from 'mobx-utils';
import * as moment from 'moment';
import { AuthService } from '../services/Auth.service';
import { userApiService, packageApiService, meApiService } from '../services';
import { NewPackage, User, NewPackageWithLessons, UsersAvailability, UsersRate, UsersMarker, NewLessonsForPackage, NewLessonsForPackageAvailability } from '../services/api';
import NewPackageTypeEnum = NewPackage.TypeEnum;
import UserStore from './User.store';
import PackageStore from './Package.store';

export enum LessonsDurationEnum {
    HALF_HOURS = 0.5,
    HOUR = 1,
    HOUR_HALF = 1.5,
}

export interface UserWithMarkers extends User {
    markers?: UsersMarker[];
}
export interface CreatePackageWizardFormStep1 {
    plan: NewPackage.TypeEnum;
    title: string;
    startWeek: string;
    student: string;
    numberWeeks: number | string;
    numberHours: number | string;
}

export interface CreatePackageWizardFormStep2 {
    students: number[];
    teacher: string;
}
export class PackageCreateStore {
    @observable selectedPlan: NewPackageTypeEnum = NewPackageTypeEnum.INDIVIDUAL;
    @observable packageStepsCounter: number = 0;
    @observable maxPageCountInStepper: number = 2;
    @observable lessonsDuration: LessonsDurationEnum = LessonsDurationEnum.HOUR;
    @observable selectedStudentsId: number[] = [];
    @observable multiplier: number = 1;
    @observable userId: number;
    @observable selectedTime: string[] = [];
    @observable createPackageResult: boolean = false;
    @observable numberHours: number = 1;
    @observable maxNumberHours: number = 20;
    @observable minNumberHours: number = 1;
    @observable numberWeeks: number = 1;
    @observable maxNumberWeeks: number = 30;
    @observable minNumberWeeks: number = 1;
    @observable startWeek: string;
    @observable name: string;
    @observable id: number;
    @observable lessonsCountToReschedule: number = null;

    @observable lessons$: ILazyObservable<User> = lazyObservable((sink) => {
        if (!this.userId) {
            sink(undefined);
            return;
        }
        this.userApi().usersIdGet(this.userId, 'lessons').then((response) => {
            sink(response.data);
        });
    });

    @observable students$: ILazyObservable<User[]> = lazyObservable((sink) => {
        this.meApi().meRelationsGet('STUDENT').then((response) => {
            sink(response.data);
        });
    });

    @observable teachers$: ILazyObservable<UserWithMarkers[]> = lazyObservable((sink) => {
        const query: string = `{"markers": [["code", "=", "${this.selectedPlan}"]]}`;
        this.userApi().usersGet(0, 30, query, undefined, undefined, 'markers', undefined, undefined, undefined).then((response) => {
            sink(response.data);
        });
    });

    @observable availableTime$: ILazyObservable<UsersAvailability[]> = lazyObservable((sink) => {
        if (!this.userId) {
            sink(undefined);
            return;
        }
        this.userApi().usersIdAvailabilitiesGet(this.userId, 0, 100).then((response) => {
            sink(response.data);
        });
    });

    @observable userRate$: ILazyObservable<UsersRate[]> = lazyObservable((sink) => {
        if (!this.userId) {
            sink(undefined);
            return;
        }
        this.userApi().usersIdRateGet(this.userId, 0, 100).then((response) => {
            sink(response.data);
        });
    });

    constructor(
        protected authService = AuthService(),
        protected userApi = userApiService,
        protected packageApi = packageApiService,
        protected meApi = meApiService,
        protected packageStore = PackageStore,
    ) {
    }

    @action('init') init(): void {
        this.createPackageResult = false;
        this.packageStepsCounter = 0;
        this.id = undefined;
        this.changeSelectedPlan(UserStore.trialMode ? NewPackage.TypeEnum.TRIAL : NewPackage.TypeEnum.INDIVIDUAL);
    }

    @action('setUserId') setUserId(id: number): void {
        this.createPackageResult = false;
        this.userId = id;
        this.setSelectedTime([]);
    }

    @action('selectTime') selectTime(time: string): void {
        const times: string[] = this.selectedTime.slice();
        const exist: number = times.indexOf(time);

        if (exist === -1) {
            if (this.needToChooseLessonsCount > 0) {
                times.push(time);
            }
        } else {
            times.splice(exist, 1);
        }
        this.setSelectedTime(times);
    }

    @action('setSelectedTime') setSelectedTime(times: string[]): void {
        this.selectedTime = times;
    }

    @action('createPackageSuccess') createPackageSuccess(): void {
        this.createPackageResult = true;
    }

    @action('clearCreatePackageStore') clearCreatePackageStore(): void {
        this.init();
        this.setUserId(null);
        this.setStartWeek(null);
        this.clearSelectedStudents();
        this.setNumberWeeks('' + this.minNumberWeeks);
        this.setNumberHours('' + this.maxNumberHours);
        this.setNeedToChooseLessonsCount(null);
        this.setName('');
    }

    @action('createPackage') async createPackage(data: NewPackage): Promise<void> {
        const response = await this.packageApi().packagesPost(data);
        this.packageStore.packagesExtended$.refresh();
        await this.loadPackage(response.data.id);
        this.incrementPackageStepsCounter();
    }
    @action('schedulePackage') async schedulePackage(availability: NewLessonsForPackageAvailability[]): Promise<void> {
        console.log(this.id);
        const response = await this.packageApi().packagesIdSchedulePost(this.id, {availability, teacher_id: this.userId});
        this.incrementPackageStepsCounter();
    }

    @action('loadPackage') async loadPackage(id: number): Promise<void>  {
        await this.packageApi().packagesIdGet(id).then((res) => {
            runInAction(() => {
                console.log('in run in action');
                this.changeSelectedPlan(res.data.type);
                this.setNumberHours(res.data.number_of_hours.toString());
                this.setNumberWeeks(res.data.number_of_weeks.toString());
                this.setId(res.data.id);
            });
        });
        console.log(this);
    }

    @action('setId') setId(id: number): void {
        this.id = id;
    }

    @action('setStartWeek') setStartWeek(startWeek: string): void {
        this.startWeek = startWeek;
    }

    @action('setName') setName(name: string): void {
        this.name = name;
    }

    @action('changeSelectedPlan') changeSelectedPlan(value: NewPackageTypeEnum): void {
        if (NewPackageTypeEnum.TRIAL === value) {
            this.lessonsDuration = LessonsDurationEnum.HOUR;
            this.setNumberWeeks('2');
            this.setNumberHours('1');
            this.setStartWeek(moment().format('YYYY-MM-DD HH:mm:ss'));
        }
        this.clearSelectedStudents();
        this.selectedPlan = value;
    }

    @action('changeLessonsDuration') changeLessonsDuration(value: LessonsDurationEnum): void {
        this.lessonsDuration = value;
        this.setSelectedTime([]);
    }

    @action('incrementPackageStepsCounter') incrementPackageStepsCounter(): void {
        this.packageStepsCounter++;
    }
    @action('setPackageStepsCounter') setPackageStepsCounter(counter: number): void {
        this.packageStepsCounter = counter;
    }
    @action('setStudentAsSelected') setStudentAsSelected(id: number): void {
        const selectedStudents: number[] = this.selectedStudentsId.slice();

        if (selectedStudents.includes(id)) {
            return;
        }
        selectedStudents.push(id);
        this.selectedStudentsId = selectedStudents;
    }

    @action('clearSelectedStudents') clearSelectedStudents(): void {
        this.selectedStudentsId = [];
    }

    @action('deleteFromSelectedStudents') deleteFromSelectedStudents(id: number): void {
        const index: number = this.selectedStudentsId.findIndex((el) => el === id);
        this.selectedStudentsId.splice(index, 1);
    }

    @action('setNumberHours') setNumberHours(value: string): void {
        const valueAsNumber: number = parseFloat(value);
        const isNumber: boolean = !isNaN(valueAsNumber);

        switch (true) {
            case (isNumber && valueAsNumber >= this.maxNumberHours): {
                this.numberHours = this.maxNumberHours;
                break;
            }
            case (isNumber && valueAsNumber <= this.minNumberHours): {
                this.numberHours = this.minNumberHours;
                break;
            }
            case (isNumber): {
                this.numberHours = valueAsNumber;
                break;
            }
        }
    }

    @action('setNumberWeeks') setNumberWeeks(value: string): void {
        const valueAsNumber: number = parseFloat(value);
        const isNumber: boolean = !isNaN(valueAsNumber);

        switch (true) {
            case (isNumber && valueAsNumber >= this.maxNumberWeeks): {
                this.numberWeeks = this.maxNumberWeeks;
                break;
            }
            case (isNumber && valueAsNumber <= this.minNumberWeeks): {
                this.numberWeeks = this.minNumberWeeks;
                break;
            }
            case (isNumber): {
                this.numberWeeks = valueAsNumber;
                break;
            }
        }
    }

    @action('setNeedToChooseLessonsCount') setNeedToChooseLessonsCount(value: number): void {
        this.lessonsCountToReschedule = value;
    }

    @computed get isLastPageInStepper(): boolean {
        return this.packageStepsCounter >= this.maxPageCountInStepper;
    }

    @computed get totalPackagePriceCalc(): number {
        const usersRate$: UsersRate[] = this.userRate$.current();
        const defaultRate: number = 1;
        const currentRate: number = (
            usersRate$ && usersRate$.length
                ? usersRate$[usersRate$.length - 1].individual_rate
                : defaultRate
        );

        return this.multiplier * this.lessonsDuration * this.numberHours * currentRate;
    }

    @computed get selectedStudents(): number[] {
        return Array.from(this.selectedStudentsId);
    }

    @computed get needToChooseLessonsCount(): number {
        const choosedTime: number = this.selectedTime.length;
        // const needToRound: boolean = this.numberHours % this.lessonsDuration !== 0;
        const lessonsCount: number = Math.ceil(this.numberHours  / this.numberWeeks / this.lessonsDuration);

        if (this.lessonsCountToReschedule) {
            return this.lessonsCountToReschedule;
        }

        // if (needToRound) {
        //     lessonsCount = Math.floor(lessonsCount) + 1;
        // }

        return lessonsCount - choosedTime;
    }

    @computed get disableSubmitButton(): boolean {
        return !(this.needToChooseLessonsCount === 0);
    }
}

const packageCreateStore = new PackageCreateStore();

reaction(() => packageCreateStore.userId, (userId) => {
    if (userId) {
        packageCreateStore.availableTime$.refresh();
        packageCreateStore.userRate$.refresh();
        packageCreateStore.lessons$.refresh();
        packageCreateStore.init();
    }
    if (packageCreateStore.createPackageResult && packageCreateStore.isLastPageInStepper) {
        this.clearCreatePackageStore();
    }
});
reaction(() => UserStore.meId, (userId: number) => {
    packageCreateStore.init();
});
export default packageCreateStore;
