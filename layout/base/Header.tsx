import * as React from 'react';
import { Link } from 'react-router-dom';
import HeaderLogo from './Header/HeaderLogo';
import HeaderProfile from './Header/HeaderProfile';
import HeaderNotify from './Header/HeaderNotify';
import HeaderMenuBar from './Header/HeaderMenuBar';

import transl from '../../utils/t.decorator';
import { GenericProps } from '../../interfaces/GenericProps.interface';
import HeaderButtonsBar from './Header/HeaderButtonsBar';

export interface HeaderProps extends GenericProps {
}

@transl()
export class Header extends React.Component<HeaderProps, any> {
    render() {
        const { t } = this.props;
        return (
            <header id="header">
                <div className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="col-md-5">
                                    <HeaderLogo />
                                </div>
                                <div className="col-md-6">
                                    <HeaderButtonsBar />
                                </div>
                            </div>
                            <div className="col-md-4 .col-md-offset-4">
                                <HeaderNotify />
                                <HeaderProfile />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <HeaderMenuBar />
                </div>
            </header>
        );
    }
}
